ARC=/archive/ug/unige/dmg/ikolpako/hic21/snps/

all: TwN/snps.vcf TwT/snps.vcf

%/snps.vcf:
	mkdir -p $(@D)
	if [ -e ${ARC}/$@ ]; then cp ${ARC}/$@ $@; else make -f from-haplo.mk $@; fi

.PHONY: clean
clean:
	rm -rf TwN/ TwT/
