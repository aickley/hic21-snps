SRC=/archive/ug/unige/medgen/gstamoul/DISCO_TR/final_hg_19_corrected
ARC=/archive/ug/unige/dmg/ikolpako/hic21/sns

all: TwT/snps.vcf TwN/snps.vcf ${ARC}/TwT/snps.vcf ${ARC}/TwN/snps.vcf

tag_TwN := NT
tag_TwT := T21

%/bychr:
	mkdir -p $@
	$(eval TAG := $(tag_$(@D)))
	find ${SRC} -name '$(TAG)*chr*.phased.haps' | xargs -L 1 -I{} cp {} $@
	rename ${TAG} '' $@/*.phased.haps

%/all.phased.haps: %/bychr
	find $< -name '*.phased.haps' | sort -V | xargs -I{} sh -c 'cat {} >> $@'

%/snps.vcf: %/all.phased.haps
	# only informative SNPs are retained by the AWK script
	awk -f haps2vcf.awk $< >$@

${ARC}/%/snps.vcf: %/snps.vcf
	mkdir -p $(@D)
	cp $< $@

.PHONY: clean
clean:
	rm -rf 'Tw{T,N}/bychr'
	rm -rf 'Tw{T,N}/all.phased.haps'
