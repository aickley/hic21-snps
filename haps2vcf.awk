BEGIN {OFS="\t"}

BEGIN {print "#CHROM","POS","ID","REF","ALT","QUAL","FILTER","INFO","FORMAT","TWIN","FATHER","MOTHER"}
$6 != $7 {print "chr"$1, $3, $2, $4, $5, ".", "PASS", ".", "GT", $6"|"$7, $8"|"$9, $9"|"$10}                                                                                                                                    
