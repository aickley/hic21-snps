# hic21-snps

Generating SNPs for twin samples from haplotyping analysis results
(part of [hic21](https://bitbucket.org/aickley/hic21-meta/) pipeline).

## Usage 

Running `make` will ensure that the current directory contains the files
`Tw{T,N}/snps.vcf` with SNPs produced from haplotyping analysis of trisomic
and normal twin respectively (they are nearly identical as expected).

These files will be either copied from the archive (if found) or generated
from the the haplotyping analysis results and stored in the archive
at `$ARCHIVE/hic21/snps`.

## Dependencies

### Data

None if cached results are found else haplotyping results.

### Software

Trivial (`awk`, `make`).

## Paths

  *  Haplotyping results: `/archive/ug/unige/medgen/gstamoul/DISCO_TR/final_hg_19_corrected`
